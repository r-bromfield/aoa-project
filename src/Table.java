import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.border.EmptyBorder;
import java.awt.ScrollPane;
import javax.swing.JTable;

public class Table extends JFrame {

	private JPanel contentPane;
	private JTable table;
	private JScrollPane spTableScroll;
	private final static String[] columnHeaders = { "Course Code", "Occurence", "Day", "Time", "Teacher" };

	/**
	 * Launch the application.
	 */
	

	/**
	 * Create the frame.
	 */
	public Table(Object[][] data) {
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setTitle("Schedule");
		setBounds(100, 100, 783, 392);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		table = new JTable();
		spTableScroll = new JScrollPane();

		
		
		table = new JTable(data, columnHeaders);
		table.setAutoCreateRowSorter(true);
		spTableScroll.add(table);
		spTableScroll.setViewportView(table);
		
		contentPane.add(spTableScroll, BorderLayout.CENTER);
		
		
		
	}

}
