import java.awt.Dimension;
import java.awt.EventQueue;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Scanner;

import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTable;



public class Driver {

	static Object[][] data = new Object[20][5];
	static String[][] schedule = new String[20][2];
	static Module[] objArray = new Module[20];
	static int loop=0;


	public static void dayCheck(Module o, String day) {

		if(o.getModoccurence().getDay()==day) {
			schedule[loop][0]=o.display();
			data[loop][0]=o.getModuleCode();
			data[loop][1]=o.getModoccurence().getCode();
			data[loop][2]=o.getModoccurence().getDay();
			data[loop][3]=o.getModoccurence().getStartTime();
			loop++;				

		}

	};

	public static String graduates[]= {
			"Clayton",
			"Clayton",
			"DeCastillo",
			"DeCastillo",
			"Lynch",
			"Lynch",
			"Major",
			"Major",
			"Moore",
			"Moore",
			"Ragsdale",
			"Ragsdale",
			"Rakes",
			"Rakes",
			"Rookwood",
			"Rookwood",
			"Russel",
			"Russel",
			"Williams",
			"Williams"
	};

	public static String modules[]= {
			"CIT4020 UM1","CIT4020 UE1","CIT3003 UM1","CIT3003 UM2","CIT3003 UN1",
			"CMP3011 UM1","CMP3011 UM2","CMP3011 UM3","CMP2006 UM1","CMP1005 UM1",
			"CMP1005 UN1","CMP1005 UE1","CIT3006 UN2","CIT3006 UM1","CIT3006 UE1",
			"CMP4011 UE1","CMP4011 UE2","CMP4011 UN1","CIT4004 UM1","CIT4004 UM2"
	};

	static String timeTable[][] = new String [20][2];



	public static void main(String[] args) {
		


		/*---------------------- OBJECT CREATION --------------------------*/
		Module obj1 = new Module("CIT4020", new Occurence("UM1","Monday","8am"));
		Module obj2 = new Module("CIT4020", new Occurence("UE1","Friday","3pm"));

		Module obj3 = new Module("CIT3003", new Occurence("UM1","Tuesday","8am"));
		Module obj4 = new Module("CIT3003", new Occurence("UM2","Wednesday","10am"));
		Module obj5 = new Module("CIT3003", new Occurence("UN1","Monday","6pm"));

		Module obj6 = new Module("CMP3011", new Occurence("UM1","Thursday","8am"));
		Module obj7 = new Module("CMP3011", new Occurence("UM2","Tuesday","10am"));
		Module obj8 = new Module("CMP3011", new Occurence("UM3","Friday","8am"));


		Module obj9 = new Module("CMP2006", new Occurence("UM1","Tuesday","10am"));

		Module obj10 = new Module("CMP1005", new Occurence("UN1","Monday","12pm"));
		Module obj11 = new Module("CMP1005", new Occurence("UE1","Tuesday","2pm"));
		Module obj12 = new Module("CMP1005", new Occurence("UN2","Friday","8pm"));

		Module obj13 = new Module("CIT3006", new Occurence("UN1","Wednesday","1pm"));
		Module obj14 = new Module("CIT3006", new Occurence("UM1","Thursday","9am"));
		Module obj15 = new Module("CIT3006", new Occurence("UE1","Monday","6pm"));

		Module obj16 = new Module("CMP4011", new Occurence("UE1","Thursday","5pm"));
		Module obj17 = new Module("CMP4011", new Occurence("UE2","Tuesday","3pm"));
		Module obj18 = new Module("CMP4011", new Occurence("UN1","Wednesday","12am"));

		Module obj19 = new Module("CIT4004", new Occurence("UM1","Monday","9am"));
		Module obj20 = new Module("CIT4004", new Occurence("UM2","Tuesday","11am"));

		/*---------------------- OBJECT ASSIGNMENT --------------------------*/

		objArray[0]=obj1;
		objArray[1]=obj2;
		objArray[2]=obj3;
		objArray[3]=obj4;
		objArray[4]=obj5;
		objArray[5]=obj6;
		objArray[6]=obj7;
		objArray[7]=obj8;
		objArray[8]=obj9;
		objArray[9]=obj10;
		objArray[10]=obj11;
		objArray[11]=obj12;
		objArray[12]=obj13;
		objArray[13]=obj14;
		objArray[14]=obj15;
		objArray[15]=obj16;
		objArray[16]=obj17;
		objArray[17]=obj18;
		objArray[18]=obj19;
		objArray[19]=obj20;






		/*---------------------- ORDER BY DAY IN ARRAY --------------------------*/
		for (int row = 0; row < 20; row++) {
			dayCheck(objArray[row],"Monday");
		}
		for (int row = 0; row < 20; row++) {
			dayCheck(objArray[row],"Tuesday");
		}
		for (int row = 0; row < 20; row++) {
			dayCheck(objArray[row],"Wednesday");
		}
		for (int row=0; row < 20; row++) {
			dayCheck(objArray[row],"Thursday");
		}
		for (int row=0; row < 20; row++) {
			dayCheck(objArray[row],"Friday");
		}



		/*---------------------- MATRIX --------------------------*/
		double cost[][]=  
		   {{2, 2, 4, 4, 4, 1, 1, 1, 3, 2, 2, 2, 5, 5, 5, 5, 5, 5, 5, 5},
			{2, 2, 4, 4, 4, 1, 1, 1, 3, 2, 2, 2, 5, 5, 5, 5, 5, 5, 5, 5},
			{3, 3, 3, 3, 3, 4, 4, 4, 1, 3, 3, 3, 5, 5, 5, 5, 5, 5, 4, 4},
			{3, 3, 3, 3, 3, 4, 4, 4, 1, 3, 3, 3, 5, 5, 5, 5, 5, 5, 4, 4},
			{2, 2, 3, 3, 3, 2, 2, 2, 1, 3, 3, 3, 4, 4, 4, 4, 4, 4, 4, 4},
			{2, 2, 3, 3, 3, 2, 2, 2, 1, 3, 3, 3, 4, 4, 4, 4, 4, 4, 4, 4},
			{1, 1, 4, 4, 4, 2, 2, 2, 5, 1, 1, 1, 3, 3, 3, 2, 2, 2, 2, 2},
			{1, 1, 4, 4, 4, 2, 2, 2, 5, 1, 1, 1, 3, 3, 3, 2, 2, 2, 2, 2},
			{1, 1, 1, 1, 1, 4, 4, 4, 4, 2, 2, 2, 3, 3, 3, 3, 3, 3, 5, 5},
			{1, 1, 1, 1, 1, 4, 4, 4, 4, 2, 2, 2, 3, 3, 3, 3, 3, 3, 5, 5},
			{1, 1, 3, 3, 3, 1, 1, 1, 5, 4, 4, 4, 1, 1, 1, 1, 1, 1, 2, 2},
			{1, 1, 3, 3, 3, 1, 1, 1, 5, 4, 4, 4, 1, 1, 1, 1, 1, 1, 2, 2},
			{3, 3, 1, 1, 1, 2, 2, 2, 5, 3, 3, 3, 1, 1, 1, 1, 1, 1, 1, 1},
			{3, 3, 1, 1, 1, 2, 2, 2, 5, 3, 3, 3, 1, 1, 1, 1, 1, 1, 1, 1},
			{3, 3, 4, 4, 4, 3, 3, 3, 5, 5, 5, 5, 1, 1, 1, 1, 1, 1, 3, 3},
			{3, 3, 4, 4, 4, 3, 3, 3, 5, 5, 5, 5, 1, 1, 1, 1, 1, 1, 3, 3},
			{4, 4, 1, 1, 1, 3, 3, 3, 2, 2, 2, 2, 5, 5, 5, 5, 5, 5, 5, 5},
			{4, 4, 1, 1, 1, 3, 3, 3, 2, 2, 2, 2, 5, 5, 5, 5, 5, 5, 5, 5},
			{4, 4, 3, 3, 3, 1, 1, 1, 5, 2, 2, 2, 3, 3, 3, 3, 3, 3, 1, 1},
			{4, 4, 3, 3, 3, 1, 1, 1, 5, 2, 2, 2, 3, 3, 3, 3, 3, 3, 1, 1}};

		int r = 20;
		int c = 20;


		/*---------------------- PRINT GRAD NAMES --------------------------*/
		System.out.println("Names of Graduates: ");
		for (int i=0;i<r;i++) {
			System.out.print(graduates[i] + ", ");
		}

		/*---------------------- PRINT AVAILABLE MODULES --------------------------*/
		System.out.println("\n\nList of Modules: ");
		for (int i=0;i<c;i++) {
			System.out.print(modules[i] + ", ");
		}

		/*---------------------- PRINT MATRIX --------------------------*/
		System.out.println("\n\nGrading Result Matrix:");
		for (int i = 0; i < r; i++)
		{
			for (int j = 0; j < c; j++)
			{
				System.out.print(cost[i][j] + ", ");
			}
			System.out.println();
		}

		/*---------------------- MATRIX REDUCTION RESULTS --------------------------*/
		HungarianBipartiteMatching hbm = new HungarianBipartiteMatching(cost);
		System.out.println();
		int[] result = hbm.execute();
		System.out.println("\nModule Matching: " + Arrays.toString(result));

		for (int i = 0; i < 20; i++)
		{
			for (int j = 0; j < 2; j++)
			{
				System.out.print(timeTable[i][j] + "  .  ");
			}
			System.out.println();
		}



		/*---------------------- POPULATING SCHEDULE --------------------------*/ 
		System.out.println("\n\nTIME TABLE:");


		/*---------------------- ASSIGN TEACHER TO OCC ---------------------------*/


		for (int i=0; i<20;i++) {
			for (int j=0; j<20;j++) {
				for (int k=0; k<20;k++) {
					if (schedule[i][0].contains(objArray[k].getModuleCode())) 
					{

						if ((objArray[k].getModuleCode()+" "+objArray[k].getModoccurence().getCode()).equals(timeTable[j][1])) {
							schedule[i][1]= timeTable[j][0];
							data[i][4]=timeTable[j][0];

						}

					}
				}
			}
		}


		for (int i = 0; i < 20; i++)
		{
			for (int j = 0; j < 2; j++)
			{
				System.out.print(schedule[i][j] + "    ");
			}
			System.out.println();
		}

		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Table frame = new Table(data);
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});

	} 


}